package StarStrek;
/**
 * 
 * @author timo scatturin
 *@version 1.0
 */
public class LadungNeu {
/**
 * Attribute der Klasse LadungNeu
 */
	private String bezeichnung;
	private int menge;
	
	/**
	 * impliziter Konstruktor f�r Ladung
	 */
	
	public LadungNeu() {
		
	}
	/**
	 * expliziter Konstruktor f�r Ladung
	 * @param bezeichnung
	 * @param menge
	 */
	public LadungNeu(String bezeichnung, int menge) {
		
		this.bezeichnung = bezeichnung;
		this.menge=menge;
	}
	/**
	 * Getter Bezeichnung
	 * @return
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	/**
	 * Setter Bezeichnung
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	/**
	 * 
	 * @return
	 */
	public int getMenge() {
		return menge;
	}
	/**
	 * 
	 * @param menge
	 */
	public void setMenge(int menge)
	{
		this.menge = menge;
	}
	
	
}
