package StarStrek;
/**
 * Importe der Klassen
 */
import StarStrek.LadungNeu;
import StarStrek.RaumschiffNeu;
/**
 * 
 * @author timo scatturin
 * @version 1.0
 */
public class Test {
/**
 * Mainmethode
 * @param args
 */
	public static void main(String[] args) {
		
		/**
		 * Deklarationen der Raumschiffe und Ladungen
		 */
		
		RaumschiffNeu k = new RaumschiffNeu ("IKS Hengh'ta", 1,100,100,100,100,2);
		RaumschiffNeu r = new RaumschiffNeu ("IRW Khazara",2, 100, 100, 100, 100, 2);
		RaumschiffNeu v = new RaumschiffNeu ("Ni Var",0, 80, 80, 50, 100, 5);
		
		LadungNeu ladungK1 = new LadungNeu ("Ferengi, Schneckensaft= ", 200);
		LadungNeu ladungK2 = new LadungNeu ("Bat'leth Klingonen Schwert= ", 200);
		LadungNeu ladungR1 = new LadungNeu ("Borg-Schrott= ", 5);
		LadungNeu ladungR2 = new LadungNeu ("Rote Materie= ", 2);
		LadungNeu ladungR3 = new LadungNeu ("Plasma-Waffe= ", 50);
		LadungNeu ladungV1 = new LadungNeu ("Forschungssonden= ", 35);
		LadungNeu ladungV2 = new LadungNeu ("Photonentorpedos= ", 3);
		
		/**
		 * Aufrufen der Methode addLadung zum hinzufüger der Ladungen zum jeweiligen Raumschiff
		 */
		k.addLadung(ladungK1);
		k.addLadung(ladungK2);
		
		r.addLadung(ladungR1);
		r.addLadung(ladungR2);
		r.addLadung(ladungR3);
		
		v.addLadung(ladungV1);
		v.addLadung(ladungV2);
		/**
		 * Methodenaufruf zur ersten Kampfphase / Beschuss
		 */
		k.photonentorpedosSchiessen(r);
		r.phaserkanoneAbschießen(k);
		/**
		 * Aufruf der Methode nachrichtAnAlle
		 * Spaceelfen finden Gewalt nicht logisch
		 */
		v.nachrichtAnAlle("Gewalt ist nicht logisch.");
		/**
		 * Methodenaufrufe zum Anzeigen von Status und Lagerbestand
		 */
		k.zustandsanzeige();
		k.ladungsverzeichnisAusgeben();
		/**
		 * 2.te Kampphase /Methodenaufruf
		 */
		k.photonentorpedosSchiessen(r);
		k.photonentorpedosSchiessen(r);
		/**
		 * Methodenaufrufe damit jedes Raumschiff seine Daten checken kann
		 */
		k.zustandsanzeige();
		k.ladungsverzeichnisAusgeben();
		
		r.zustandsanzeige();
		r.ladungsverzeichnisAusgeben();
		
		v.zustandsanzeige();
		v.ladungsverzeichnisAusgeben();
				
	}
}
