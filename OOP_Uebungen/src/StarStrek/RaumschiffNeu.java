package StarStrek;
/**
 * Importiert Funktion, damit Arraylisten verwendet werden k�nnen
 */
import java.util.ArrayList;
/**
 * Diese Klasse erstellt ein Raumschiff mit seinen Attributen und Methoden
 * @author timo scatturin
 * @version 1.0
 */
public class RaumschiffNeu {
	/**
	 * Deklaration der Attribute f�r das Raumschiff
	 */
	private String raumschiffname;
	private int energieversorgung;
	private int schildeInProzent;
	private int huellenInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private int anzahlPhotonentorpedos;
	public String nachricht;
	
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<LadungNeu> ladungsverzeichnis = new ArrayList<LadungNeu>();
	/**
	 * implizieter Konstruktor zum Erstellen eines Raumschiffs
	 */
	public RaumschiffNeu() {
		
	}
	/**
	 * expliziter Konstruktor zum Erstellen eines Raumschiffs
	 * @param raumschiffname
	 * @param photonentorpedos
	 * @param energieversorgung
	 * @param schildeInProzent
	 * @param huellenInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param androidenAnzahl
	 */
	public RaumschiffNeu (String raumschiffname, int photonentorpedos, int energieversorgung, int schildeInProzent, int huellenInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {
		setRaumschiffname (raumschiffname);
		setAnzahlPhotonentorpedos(photonentorpedos);
		setEnergieversorgung (energieversorgung);
		setSchildeInProzent (schildeInProzent);
		setHuellenInProzent (huellenInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		
		
		
	}
	
	/**
	 * Methode zur Zustandsanzeige eines Raumschiffs
	 */
	public void zustandsanzeige() {
		System.out.printf("Aktueller Zustand des Raumschiffs: %s\n", this.raumschiffname);
		System.out.printf("Energieversorgung: %d\n", this.energieversorgung);
		System.out.printf("Schilde: %d\n", this.schildeInProzent);
		System.out.printf("Huellen: %d\n", this.huellenInProzent);
		System.out.printf("Lebenserhaltungssysteme: %d\n", this.lebenserhaltungssystemeInProzent);
		System.out.printf("Anzahl der Photonentorpedos: %d\n", this.anzahlPhotonentorpedos);
		System.out.printf("Anzahl der Androiden: %d\n", this.androidenAnzahl);
	}
	
	/**
	 * Setter Raumschiffname
	 * @param raumschiffname
	 */
	public void setRaumschiffname (String raumschiffname){
		this.raumschiffname = raumschiffname;
	}
	/**
	 * Getter Raumschiffname
	 * @return R�ckgabe Raumschiffname
	 */
	public String getRaumschiffname(){
		return this.raumschiffname;
	}
	/**
	 * 
	 * @param energieversorgung
	 */
	public void setEnergieversorgung(int energieversorgung){
		this.energieversorgung = energieversorgung;
	}
	/**
	 * 
	 * @return
	 */
	public int getEnergieversorgung() {
		return this.energieversorgung;
	}
	/**
	 * 
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent){
		this.schildeInProzent = schildeInProzent;
	}
	/**
	 * 
	 * @return
	 */
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	/**
	 * 
	 * @param huellenInProzent
	 */
	public void setHuellenInProzent(int huellenInProzent){
		this.huellenInProzent = huellenInProzent;
	}
	/**
	 * 
	 * @return
	 */
	public int getHuellenInProzent () {
		return this.huellenInProzent;
	}
	/**
	 * 
	 * @param lebenserhaltungssystemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent (int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	/**
	 * 
	 * @return
	 */
	public int getLebenserhaltungssystemInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	/**
	 * 
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl (int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	/**
	 * 
	 * @return
	 */
	public int getandroidenAnzahl () {
		return this.androidenAnzahl;
	}
	/**
	 * 
	 * @param anzahlPhotonentorpedos
	 */
	public void setAnzahlPhotonentorpedos(int anzahlPhotonentorpedos) {
		this.anzahlPhotonentorpedos = anzahlPhotonentorpedos;
	}
	/**
	 * 
	 * @return
	 */
	public int getAnzahlPhotonentorpedos() {
		return this.anzahlPhotonentorpedos;
	}
	/**
	 * 
	 * @param ladung
	 */
	public void setLadungsverzeichnis (ArrayList<LadungNeu> ladung) {
		this.ladungsverzeichnis = ladung;
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<LadungNeu> getLadungsverzeichnis(){
		return this.ladungsverzeichnis;
	}
	/**
	 * 
	 * @param broadcastKommunikator
	 */
	public void setBroadcastKommunikator (ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getBroadcastKommunikator(){
		return this.broadcastKommunikator;
	}
	
	/**
	 * Methode Photonentorpedos abschie�en
	 * @param RaumschiffNeu
	 */
	
	 public void photonentorpedosSchiessen(RaumschiffNeu RaumschiffNeu) {
	        if (anzahlPhotonentorpedos > 0) {
	            broadcastKommunikator.add("Photonentorpedo abgeschossen.\n");
	            anzahlPhotonentorpedos = this.anzahlPhotonentorpedos - 1;
	            treffer(RaumschiffNeu);
	            nachrichtAnAlle();
	            
	        }
	        else {
	        	System.out.println("-=*Click*=- ");
	        	System.out.println("KeineMunition!");
	        	}
	        
	        }
	        
	/**
	 * Methode Treffer verzeichnen
	 * @param RaumschiffNeu
	 */
	 
	  public void treffer(RaumschiffNeu RaumschiffNeu) {
		  
	            broadcastKommunikator.add(RaumschiffNeu  + " wurde getroffen.");
	            schildeInProzent = schildeInProzent - 50;

	     

	            if (schildeInProzent < 1) {

	     

	                huellenInProzent = huellenInProzent - 50;
	                lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent - 50;

	     

	                if (huellenInProzent < 1) {

	     

	                    broadcastKommunikator.add("Lebenserhaltungssysteme von " + RaumschiffNeu + " wurden zerst�rt.");
	                }
	            }
	        }
	  
	  /**
	   * Methode Phaserkanonen abschie�en
	   * @param RaumschiffNeu
	   */
	  
	  public void phaserkanoneAbschie�en(RaumschiffNeu RaumschiffNeu) {
	        if (energieversorgung > 50) {
	            energieversorgung = energieversorgung - 50;
	            broadcastKommunikator.add("Phaserkanone abgeschossen.");
	            treffer(RaumschiffNeu);
	            nachrichtAnAlle();
	            
	        }	 
	        else {
	            System.out.println("-=*Click*=- ");
	            System.out.println("KeineMunition!");
	        }	 
	        
	    }
	  /**
	   * Methode um Nachricht an alle zu senden
	   * @param nachricht
	   */
	  public void nachrichtAnAlle(String nachricht) {
	        broadcastKommunikator.add(nachricht);	 
	    }
	  
	  
	  
	  /**
	   * addladung Methode 
	   * @param neueLadung
	   */
	  public void addLadung(LadungNeu neueLadung) {
	        ladungsverzeichnis.add(neueLadung);
	    }
	  
	  /**
	   * Methode zum Ausgeben von Nachrichten an alle (funktioniert glaube ich nicht richtig)
	   */
	  
	  public void nachrichtAnAlle() {
	        
	        for(int i = 0; i < broadcastKommunikator.size(); i++) {
	            System.out.println(broadcastKommunikator.get(i) );
	        if(i>=1)
	        	System.out.println("Gewalt ist nicht logisch");
	        
	        }
	    }
	  
	  /**
	   * Methode Ladung ausgeben, zeigt die Ladung eines Raumschiffs mit Menge und Bezeichnung an
	   */
	  
	  public void ladungsverzeichnisAusgeben() {
	       System.out.println("Laut Ladungsverzeichnis sind noch ");
	        for(int k = 0; k < ladungsverzeichnis.size(); k++) {
	            System.out.println(ladungsverzeichnis.get(k).getBezeichnung() + ladungsverzeichnis.get(k).getMenge());
	           
	           
	        }    
	        System.out.println("vorhanden.");
	  }
	    
	
	
}
